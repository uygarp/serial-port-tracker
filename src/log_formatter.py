#!/bin/python

import sys
import datetime
import text_painter as tp

default_log_file='mylog.txt'

timestamp_date_format = '%y-%m-%d'
timestamp_hour_format = '%H:%M:%S:%f'
timestamp_spacer = ' | '
timestamp_date_color = tp.COLOR.green
timestamp_hour_color = tp.COLOR.cyan
timestamp_spacer_color = tp.COLOR.bright_green

class LogWriter():
    timestamp = False
    logging = False
    log_file = ''
    __add_time = True
    filter_list = None
    def __init__(self, timestamp=False, logging=False, log_file=default_log_file, filter_list=None):
        self.timestamp = timestamp
        self.logging = logging
        self.log_file = log_file
        self.filter_list = filter_list
    def __write(self, text_formatter):
        sys.stdout.write(text_formatter.text_painted)
        if self.logging:
            f = open(self.log_file, "a")
            f.write(text_formatter.text_formatted)
            f.close()
    def handle_output(self, output):
        if self.timestamp:
            if self.__add_time:
                self.__write(DateNowTextFormatter())
            self.__add_time = (output.endswith('\n') or output.endswith('\r'))
        self.__write(LogTextFormatter(output, self.filter_list))

class TextFormatter():
    text_painted = ""
    text_formatted = ""
        
class LogTextFormatter(TextFormatter):
    def __init__(self, text, filter_list):
        self.text_painted = self.__apply_filters(text, filter_list)
        self.text_formatted = text
    def __apply_filters(self, text, filter_list):
        if filter_list is None:
            return text
        # painted_data = reconfigure_section(text)
        painted_data = text
        for k in range(len(filter_list)):
            parts = painted_data.split(filter_list[k][0])
            temp_painted_data = ""
            
            for i in range(len(parts)):
                temp_painted_data += tp.color_text(parts[i], tp.COLOR.normal)
                if(i < len(parts) - 1):
                    temp_painted_data += tp.color_text(filter_list[k][0], filter_list[k][1])
            painted_data = temp_painted_data
        return painted_data
    def __clean_previous_colors(self):
        #TODO: Should be completed later.
        pass

class DateNowTextFormatter(TextFormatter):
    def __init__(self):
        text = datetime.datetime.now()
        time_date_str = text.strftime(timestamp_date_format)
        time_hour_str = text.strftime(timestamp_hour_format)[:-3]
        time_spacer_str = timestamp_spacer
        self.text_formatted = time_date_str + ' ' + time_hour_str + timestamp_spacer
    
        time_date_str = tp.color_text(time_date_str, timestamp_date_color)
        time_hour_str = tp.color_text(time_hour_str, timestamp_hour_color)
        time_spacer_str = tp.color_text(timestamp_spacer, timestamp_spacer_color)
        self.text_painted = time_date_str + ' ' + time_hour_str + time_spacer_str
        