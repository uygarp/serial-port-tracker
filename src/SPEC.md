# Serial Port Tracker
*[cmd: eser --default, -d]*
- Runs default profile.

*[cmd: eser]*
- Select a profile, and contains run button.
Includes also:
- Show settings button

## ESer Settings
[cmd: eser --settings, -s]

- There will be 3 tabs here: Main, Output Colouring, Input Controller, Keybord Shortcuts.   

### Main Settings

- **Profile:** Select one from each setting or none. Connection should not be none. There should be a 'add' button on the right side. This should represent a dialog which offer the new profile name, and an option to select a copy-from profile. (radio button: none, or select from (combobox: existing profiles))
    - Terminal: (default: system terminal) User could select another terminal.
    - Timestamp: (default: disabled) User can enable this, and also format timestamp view. (date format, color, background)
    - Write-to-file: (default: disabled) User can enable this. Also, a timestamp adding option (default: disabled) should be activated when enabled. 
- **Connection:**
    - Port name: (default: ??) There could be a combobox that was updated connected serial ports automatically.
    - Baud rate:

### Output Coloring
- For output
- For input ???

### Input Controller
- *Each button:* name, command, description(tooltip), position, terminal color, background, button color, background, keyboard shortcut
- ** Automated Menu **

### Keyboard Shortcuts
- Exit terminal
​
