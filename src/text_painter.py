#!/bin/python

from enum import Enum

class COLOR(Enum):
    normal          = '0'
    black           = '0;30'
    red             = '0;31'
    green           = '0;32'
    yellow          = '0;33'    
    blue            = '0;34'
    purple          = '0;35'
    cyan            = '0;36'
    bright_gray     = '0;37'
    dark_gray       = '1;30'    
    bright_red      = '1;31'
    bright_green    = '1;32'
    bright_yellow   = '1;33'
    bright_blue     = '1;34'
    bright_purple   = '1;35'
    bright_cyan     = '1;36'
    white           = '1;37'

   
def color_text(text, color):
    return "\033[" + color.value + "m" + text + "\033[" + COLOR.normal.value + "m"
