#!/bin/python

import sys
import threading
import serial
import time
import os
import sys
import termios
import fcntl
import log_formatter as lf
import text_painter as tp
import argparse
import serial_com as sc
from serial.tools.list_ports_linux import comports

PROG_NAME   = "ESer"
VERSION     = "1.0" 

port = '/dev/ttyUSB1'
baud = 115200
timestamp=False
capture=False

default_capture_file='/home/uygarp/logger.txt'


filter_list =   [
                ["HW u8Key", tp.COLOR.yellow],
                ["u8Key=", tp.COLOR.cyan],
                ["u8System=", tp.COLOR.blue],
                ["key - data", tp.COLOR.green],
                ["*** KeyName=", tp.COLOR.purple],
                ["ecl/servicefinder", tp.COLOR.blue],
                ]

#DEVICE=/dev/ttyUSB1
#BAUDRATE=115200
#LOGFILE=/home/uygarp/ttyDump.log
#LOGFILE = ""

# TODO's: 
# Filter text(key), whole-word? case-sensitive? regular expression?
# Format: Color, backcolor, bold, italic
# Color whole-line ? True: False
# Disable RX coloring
# Disable a line contains pattern
# Command parameters:
# --settings: Opens settings window 
# timestamp, on-screen? in file? (is it necessary?)
# TODO: Think about timestamp choices. Are they necessary both?
# log to file
# TODO's inside source code
#+++ TODO: Console memu for serial port name. Allow user to choose a port
# TODO: Give user a port parameter. If connection cannot succeed, open console menu for port select.

# For Performance Issues
#LINE_READ_ENABLED = False

def get_user_int():
    try:
        sys.stdin = open('/dev/tty')
        chr =raw_input("Please select: ")
        user_int = int(chr)
    except ValueError:
        print "Error! Invalid selection..."
        exit()
    return user_int

if __name__ == '__main__':
    
    ap = argparse.ArgumentParser()
    ap.add_argument("-t", "--time", help="Timestamp adding.file", action="store_true")
    #ap.add_argument("-f", "--file", help="Capture to file", nargs='?', default=False)
    #ap.add_argument("-r", "--rules", help="Rules file name", type=argparse.FileType('r'))
    ap.add_argument("-r", "--rules", help="Rules file name", type=argparse.FileType('r'))
    ap.add_argument("-c", "--commands", help="Commands screen", type=argparse.FileType('r'))
    ap.add_argument("-v", "--version", action='version', version='{} {}'.format(PROG_NAME, VERSION))
    args = vars(ap.parse_args())
    
    timestamp = args["time"]
    capture = False #(args["capture"] is not False)
    capture_file = default_capture_file
    if capture and args["capture"] is not None:
        capture_file = args["capture"]
    rules_file = args["rules"]
    
    ser_dev_list = sc.SerialComm().list()
    
    for ser_dev in ser_dev_list:
        print (ser_dev_list.index(ser_dev) + 1), ": ", ser_dev.device, "[", ser_dev.baudrate, "]"
    
    sel_index = get_user_int()
        
    if(sel_index > len(ser_dev_list) or sel_index <= 0):
        print "Error! Invalid selection..."
        exit()
        
    ser = ser_dev_list[sel_index -1]
    print "Connecting to ", ser.device, "[", ser.baudrate, "] ..."
    print "---------------------------------------------------------------------"
    log_writer = lf.LogWriter(timestamp, capture, capture_file, filter_list)
    ser.connect(log_writer.handle_output)
