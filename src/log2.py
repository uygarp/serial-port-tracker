#!/bin/python

import sys
import threading
import serial
import time
import os
import sys
import termios
import fcntl
import log_formatter as lf
import text_painter as tp
import argparse
from serial.tools.list_ports_linux import comports

PROG_NAME   = "ESer"
VERSION     = "1.0" 

port = '/dev/ttyUSB2'
baud = 9600
timestamp=False
capture=False

default_capture_file='/home/uygarp/mylog2.txt'


filter_list =   [
                ["HW u8Key", tp.COLOR.yellow],
                ["u8Key=", tp.COLOR.cyan],
                ["u8System=", tp.COLOR.blue],
                ["key - data", tp.COLOR.green],
                ["*** KeyName=", tp.COLOR.purple],
                ]

#DEVICE=/dev/ttyUSB1
#BAUDRATE=115200
#LOGFILE=/home/uygarp/ttyDump.log
#LOGFILE = ""

# TODO's: 
# Filter text(key), whole-word? case-sensitive? regular expression?
# Format: Color, backcolor, bold, italic
# Color whole-line ? True: False
# Disable RX coloring
# Disable a line contains pattern
# Command parameters:
# --settings: Opens settings window 
# timestamp, on-screen? in file? (is it necessary?)
# TODO: Think about timestamp choices. Are they necessary both?
# log to file
# TODO's inside source code
# TODO: Console memu for serial port name. Allow user to choose a port
# TODO: Give user a port parameter. If connection cannot succeed, open console menu for port select.

# For Performance Issues
#LINE_READ_ENABLED = False
PORT_READ_DELAY = .001
PORT_WRITE_DELAY = .01

def read_from_port(ser):
    log_writer = lf.LogWriter(timestamp, capture, capture_file, filter_list)
    while ser.isOpen():
        #input_len = ser.inWaiting()
        if ser.inWaiting() > 0:
#              # TODO: implementation with readall()
#             if LINE_READ_ENABLED:
#                 buffer = ser.readline()
#             else:
#                 buffer = ""
#                 buffer_len = 0
#                 while buffer_len < input_len:
#                     buffer += ser.read(input_len - buffer_len)
#                     buffer_len = len(buffer)
            
            log_writer.handle_output(ser.readline())
            
        time.sleep(PORT_READ_DELAY)
 
def write_to_port(ser):
    while ser.isOpen():
        buffer = getch()
        ser.write("%s" % buffer)

def getch():
    fd = sys.stdin.fileno()
    
    oldterm = termios.tcgetattr(fd)
    newattr = termios.tcgetattr(fd)
    newattr[3] = newattr[3] & ~termios.ICANON & ~termios.ECHO
    termios.tcsetattr(fd, termios.TCSANOW, newattr)
    
    oldflags = fcntl.fcntl(fd, fcntl.F_GETFL)
    fcntl.fcntl(fd, fcntl.F_SETFL, oldflags | os.O_NONBLOCK)

    try:
        while True:
            time.sleep(PORT_WRITE_DELAY)
            try:
                c = sys.stdin.read(1)
                break
            except IOError: pass
    finally:
        termios.tcsetattr(fd, termios.TCSAFLUSH, oldterm)
        fcntl.fcntl(fd, fcntl.F_SETFL, oldflags)
    return c

import serial.tools.list_ports   # import serial module
import serial
from serial.serialutil import SerialException
import subprocess
import shlex
from subprocess import Popen, PIPE

def get_baudrate(device):
    command = 'stty < {0}'.format(device)
    command = 'stty</dev/ttyUSB1|grep speed'
    print command
    cmd_list = shlex.split(command)
    print cmd_list
    proc_retval = subprocess.check_output(cmd_list)
    baudrate = int(proc_retval.split()[1])
    return baudrate

import subprocess
import shlex

def get_baudrate2(device):
    command = 'stty < {0}'.format(device)
    proc_retval = subprocess.check_output(shlex.split(command))
    baudrate = int(proc_retval.split()[1])
    return baudrate

def get_baudrate3(device):
    ser = serial.Serial(device)
    ser.timeout = 0.01
    for baudrate in ser.BAUDRATES:
        if 9600 <= baudrate <= 115200:
            ser.baudrate = baudrate
            #ser.write(packet)
            resp = ser.read()
            if resp != '':
                break
    if ser.baudrate > 115200:
        raise RuntimeError("Couldn't find appropriate baud rate!")
    return ser.baudrate

if __name__ == '__main__':
    
    ap = argparse.ArgumentParser()
    ap.add_argument("-t", "--time", help="Timestamp adding.file", action="store_true")
    ap.add_argument("-c", "--capture", help="Capture to file", nargs='?', default=False)
    #ap.add_argument("-r", "--rules", help="Rules file name", type=argparse.FileType('r'))
    ap.add_argument("-r", "--rules", help="Rules file name", type=argparse.FileType('r'))
    ap.add_argument("-v", "--version", action='version', version='{} {}'.format(PROG_NAME, VERSION))
    args = vars(ap.parse_args())
    
    timestamp = args["time"]
    capture = (args["capture"] is not False)
    capture_file = default_capture_file
    if capture and args["capture"] is not None:
        capture_file = args["capture"]
    rules_file = args["rules"]
    
    # Getting Port
    comPorts = list(serial.tools.list_ports.comports())    # get list of all devices connected through serial port
    print 'List of devices connected on com ports : ', comPorts
    numPortsUsed = len(comPorts)   # get number of com ports used   # get number of com ports used in system
    print 'Number of com ports used in system : ', numPortsUsed
    if numPortsUsed < 1:
        print "There is no serial port connected..."
        exit()

    for port in comPorts:
        print port.__str__()
        device = port.__str__().split()[0]
        #print "device", device
        baudrate = get_baudrate3(device)
        print "baudrate:", baudrate
         
#    exit()
    
    serial_port = serial.Serial(port, baud, timeout=0)
    serial_port.flushInput()
    serial_port.flushOutput()
    reading_thread = threading.Thread(target=read_from_port, args=(serial_port,))
    write_thread = threading.Thread(target=write_to_port, args=(serial_port,))
    
    reading_thread.start()
    write_thread.start()
