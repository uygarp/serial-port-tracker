#!/bin/python


import sys
import threading
import serial
import time
import os
import sys
import termios
import fcntl
import log_formatter as lf
import text_painter as tp
import argparse
import serial_com as sc
from serial.tools.list_ports_linux import comports

import serial.tools.list_ports   # import serial module
import serial
from serial.serialutil import SerialException
import subprocess
import shlex
from subprocess import Popen, PIPE

PORT_READ_DELAY = .001
PORT_WRITE_DELAY = .01

class SerialComm:
    __ser_dev_list = []
    def __init__(self):
        # Getting Port
        comPorts = list(serial.tools.list_ports.comports())    # get list of all devices connected through serial port
        #print 'List of devices connected on com ports : ', comPorts
        numPortsUsed = len(comPorts)   # get number of com ports used   # get number of com ports used in system
        #print 'Number of com ports used in system : ', numPortsUsed
        if numPortsUsed < 1:
            print "There is no serial port connected..."
            exit()

        for port in comPorts:
            #print port.__str__()
            device = port.__str__().split()[0]
            #baudrate = get_baudrate(device)
            ser = Serial(device)
            self.__ser_dev_list.append(ser)
            #print (self.__ser_dev_list.index(ser) + 1), ": ", ser.device, "[", ser.baudrate, "]"
    
    def list(self):
        return self.__ser_dev_list

class Serial:
    device = ""
    baudrate = 0
    brand = ""
    #__serial = instance(Serial)
    def __init__(self, device):
        self.device = device
        self.baudrate = self.__get_baudrate()
        self.__serial = serial.Serial(self.device, self.baudrate, timeout=0)
        
    def connect(self, handle_output):
        self.__serial.flushInput()
        self.__serial.flushOutput()
        reading_thread = threading.Thread(target=self.__read_from_port, args=(handle_output,))
        write_thread = threading.Thread(target=self.__write_to_port)
        
        reading_thread.start()
        write_thread.start()

    def __read_from_port(self, handle_output):
        while self.__serial.isOpen():
            #input_len = self.inWaiting()
            if self.__serial.inWaiting() > 0:
    #              # TODO: implementation with readall()
    #             if LINE_READ_ENABLED:
    #                 buffer = self.readline()
    #             else:
    #                 buffer = ""
    #                 buffer_len = 0
    #                 while buffer_len < input_len:
    #                     buffer += self.read(input_len - buffer_len)
    #                     buffer_len = len(buffer)
                handle_output(self.__serial.readline())
            time.sleep(PORT_READ_DELAY)

    def __write_to_port(self):
        while self.__serial.isOpen():
            buffer = self.__getch()
            self.__serial.write("%s" % buffer)

    def __getch(self):
        fd = sys.stdin.fileno()
    
        oldterm = termios.tcgetattr(fd)
        newattr = termios.tcgetattr(fd)
        newattr[3] = newattr[3] & ~termios.ICANON & ~termios.ECHO
        termios.tcsetattr(fd, termios.TCSANOW, newattr)
    
        oldflags = fcntl.fcntl(fd, fcntl.F_GETFL)
        fcntl.fcntl(fd, fcntl.F_SETFL, oldflags | os.O_NONBLOCK)

        try:
            while True:
                time.sleep(PORT_WRITE_DELAY)
                try:
                    c = sys.stdin.read(1)
                    break
                except IOError: pass
        finally:
            termios.tcsetattr(fd, termios.TCSAFLUSH, oldterm)
            fcntl.fcntl(fd, fcntl.F_SETFL, oldflags)
        return c

    def __get_baudrate(self):
        if self.device == "":
            return 0
            
        ser = serial.Serial(self.device)
        ser.timeout = 0.01
        for baudrate in ser.BAUDRATES:
            if 9600 <= baudrate <= 115200:
                ser.baudrate = baudrate
                #ser.write(packet)
                resp = ser.read()
                if resp != '':
                    break
        if ser.baudrate > 115200:
            raise RuntimeError("Couldn't find appropriate baud rate!")
        return ser.baudrate
